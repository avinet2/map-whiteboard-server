export namespace MWTypes {

    export interface FeatureWrapper {
        user?: string;
        mapId: any,
        feature?: GeoJSON.Feature,
        type: 'insert' | 'update' | 'delete' | 'restore',
        featureId: string | number,
        layer: {
            name: string, // 'scratch' | 'cursor'; 
            id: string
        }; 
    }

    export interface FeatureChangeWrapper {
        user?: string;
        mapId: any,
        differences: any,
        featureId: string | number,
        layer: {
            name: string, // 'scratch' | 'cursor'; 
            id: string
        }; 
    }

    export interface ConnectionInfo {
        socketId: string,
        color: string,
        message?: string,
    }

    export interface UserCredentials {
        id?: string,
        username?: string,
        password?: string,
    }

    export interface CursorInfo {
        u: string; //User
        un: string; //Username
        i?: string, //Socket id
        xy: number[], //Position
        c?: string, //Color
    }
}

