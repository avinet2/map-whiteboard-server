import { log } from "./logs";
import config from "./../config";
import path = require("path");
import mongoose from 'mongoose';

const multer = require("multer");
const fs = require("fs");

const mapStorage = multer.diskStorage({
  destination: function (req, file, cb) {
    log('UPLOAD[mapStorage destination]');
    if (!fs.existsSync(config.uploadDir)) {
      fs.mkdirSync(config.uploadDir);
    }
    cb(null, path.resolve(config.uploadDir));
  },
  filename: function (req, file, cb) {
    log('UPLOAD[mapStorage filename]');
    let fileNameExt = path.extname(file.originalname);
    let fileName = new mongoose.Types.ObjectId().toHexString();
    cb(null, fileName + fileNameExt);
  },
});
const mapUploader = multer({ storage: mapStorage }).single("file");
export default mapUploader;