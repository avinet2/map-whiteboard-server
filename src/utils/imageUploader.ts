import { log } from "./logs";
import path = require("path");
import config from "./../config";

const multer = require("multer");
const fs = require("fs");

const profileDir = path.resolve(path.join(config.uploadDir, 'profile'));
const imageStorage = multer.diskStorage({
  destination: function (req, file, cb) {
    log('UPLOAD[imageStorage destination]');
    if (!fs.existsSync(profileDir)) {
      fs.mkdirSync(profileDir);
    }
    cb(null, profileDir);
  },
  filename: function (req, file, cb) {
    log('UPLOAD[imageStorage filename]');
    let fileType = "";
    if (file.mimetype === "image/png") {
      fileType = "png";
    }
    if (file.mimetype === "image/jpeg") {
      fileType = "jpg";
    }
    cb(null, "image-" + Date.now() + "." + fileType);
  },
});
const imageUploader = multer({ storage: imageStorage }).single("file");
export default imageUploader;