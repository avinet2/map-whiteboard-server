export function log(tag: string, extraData?: any, message?: string) {
  message = message != undefined ? message : "";
  const sExtra = extraData != undefined ? JSON.stringify(extraData) : "";
  const optionalOutput = `${message} ${sExtra}`;
  console.log(`${new Date().toISOString()}\t${tag}\t${optionalOutput}`);
}
