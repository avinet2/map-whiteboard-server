import { MongoDBUtils as mu } from "../mongoose-utils";

export function sendRequestCompleted(res) {
  return res.status(200).json(mu.success("Request completed"));
}

export function sendSuccessData(res, data: any) {
  return res.status(200).json(mu.success(data));
}

export function sendAccessDenied(res) {
  return res.status(403).json(mu.error("Access denied"));
}

export function sendUnauthorized(res) {
  return res.status(401).json(mu.error("Unauthorized"));
}

export function sendInternalError(res, ex) {
  return res.status(500).json(mu.error(ex));
}

export function sendUnprocessableEntity(res, ex) {
  return res.status(422).json(mu.error(ex));
}


export function sendBadRequest(res, ex) {
  return res.status(400).json(mu.error(ex));
}

export function sendNotFound(res, data: any) {
  return res.status(404).json(mu.error(data));
}
