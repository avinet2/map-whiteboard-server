// Imports
import bodyParser from "body-parser";
import { UserCtrl } from "./controllers/UserCtrl";
import { Colors } from "./colors";
import { Router } from "./router";
import gju from "./gjutils";
import { MWTypes } from "../types/m-w-types";
import { MapCtrl } from "./controllers/MapCtrl";
import { MapPrivilegeCtrl } from "./controllers/MapPrivilegeCtrl";
import MapModel from "./types/Map";
import { log } from "./utils/logs";
import GeoJSONFeatureModel from "./types/GeoJSONFeature";
import { Role } from "./types/User";
import { Right } from "./types/Privilege";
import config from "./config";
import { AuthCtrl } from "./controllers/AuthCtrl";
import { userSessions } from "./volatile-data";

// Requirements
const dotenv = require('dotenv');
const app = require("express")();
const http = require("http").Server(app);
const io = require("socket.io")(http);
const setModifyThrottler = {};
const queue = require("queue");

//get environment variables
const result = dotenv.config()
if (result.error) {
    console.warn(result.error)
}
// Socket-io authentication
require("socketio-auth")(io, {
  authenticate: async (socket, data, callback) => {
    const result = await AuthCtrl.authenticateContext(socket, data);
    if(result?.success) {
      callback(null, true)
    } else if(result?.error) {
      callback(result.error, false)
    } else {
      callback(null, false)
    }
  },
  postAuthenticate: AuthCtrl.postAuthenticate,
  disconnect: AuthCtrl.disconnect,
  timeout: 2000,
});

// Enable body parsing of json and URL-encoded content
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// Setup web service REST routes
Router.configure(app);

MapCtrl.io = io;
UserCtrl.io = io;
// Feature collection
var featureCollection: GeoJSON.FeatureCollection =
  gju.createFeatureCollection();

// Cursors
var cursors: Array<{ id: string; cursor: any; color: string }> = [];

export const activeSocketsArray: Array<{
  socketId: string;
  user: string;
  username: string;
}> = [];

// Active maps
var maps: {
  [mapId: string]: {
    layers: {
      scratch: GeoJSON.FeatureCollection;
      cursor: GeoJSON.FeatureCollection;
    };
  };
} = {};

//Feature actions enum
enum featureActions {
  upsert = "upsert-feature",
  change = "change-feature",
  delete = "delete-feature",
  restore = "restore-feature",
}

// Queueing object
var featureQueues: {
  [featureId: string]: {
    q: any; //queue object
  };
} = {};

// Socket handler
io.on("connection", (socket) => {
  log("SOCKET[connection]", { "socket.id": socket.id });

  socket.on("get-connection-info", (mapId: string) => {
    log("SOCKET[get-connection-info]");
    if (userSessions[socket.id]?.mapId == mapId) {
      return;
    }
    socket.join(mapId);

    activeSocketsArray.push({
      socketId: socket.id,
      user: socket.client?.user,
      username: socket.client?.username,
    });

    userSessions[socket.id] = {
      color: Colors.getColor(),
      user: socket.client?.user,
      username: socket.client?.username,
      mapId: mapId,
    };

    if (maps[mapId] === undefined) {
      maps[mapId] = {
        layers: {
          scratch: gju.createFeatureCollection(),
          cursor: gju.createFeatureCollection(),
        },
      };
    }
    Object.keys(maps).forEach((map) => {
      setJoinedUsersCount(map);
    });

    let connectionInfo: MWTypes.ConnectionInfo = {
      socketId: socket.id,
      message: `Welcome, you are connected to Map Whiteboard Server with socket ID: ${socket.id}`,
      color: userSessions[socket.id].color,
    };
    io.to(socket.id).emit("connection-info", connectionInfo);
    socket.emit(
      "feature-collection",
      maps[userSessions[socket.id].mapId].layers.cursor
    );
    socket.emit(
      "feature-collection",
      maps[userSessions[socket.id].mapId].layers.scratch
    );
  });

  socket.on(featureActions.upsert, async (featWrap: MWTypes.FeatureWrapper) => {
    featureProcessing(featWrap, featureActions.upsert);
  });

  socket.on(featureActions.change, async (featWrap: MWTypes.FeatureChangeWrapper) => {
    featureProcessing(featWrap, featureActions.change);
  });

  socket.on(featureActions.restore, (featWrap: MWTypes.FeatureWrapper) => {
    featureProcessing(featWrap, featureActions.restore);
  });

  socket.on(featureActions.delete, (featWrap: MWTypes.FeatureWrapper) => {
    featureProcessing(featWrap, featureActions.delete);
  });

  function setModifiedBy(mapId: string, email: string) {
    if (setModifyThrottler[mapId]) {
      clearTimeout(setModifyThrottler[mapId]);
    }
    setModifyThrottler[mapId] = setTimeout(() => {
      log("APP[setModifiedBy]");
      MapModel.findByIdAndUpdate(mapId, { lastModifiedBy: email }).exec();
      delete setModifyThrottler[mapId];
    }, 500);
  }

  async function changeFeature(featChangeWrap: MWTypes.FeatureChangeWrapper) {
    try {
      log("SOCKET[change-feature]");
      const allowed = await mapAllowed(socket, featChangeWrap.mapId);
      const featureId = featChangeWrap.featureId;
      if (!allowed) {
        io.to(socket.id).emit("sync", {
          featureId,
          layer: featChangeWrap.layer,
          error: "Access denied",
        });
        return;
      }
      setFeatureChangeAuthor(socket, featChangeWrap);
      if (layerSavable(featChangeWrap.layer)) {
        MapCtrl.createLayerIfNotExists(
          featChangeWrap.mapId,
          featChangeWrap.layer,
          socket.client.user
        ); // Don't use await to no delay execution
        gju.changeFeature(featChangeWrap.mapId, featChangeWrap.layer.id, featureId, featChangeWrap.differences);
        setModifiedBy(featChangeWrap.mapId, socket.client.user);
      }
      socket.to(featChangeWrap.mapId).emit("change-feature", featChangeWrap);
      io.to(socket.id).emit("sync", {
        featureId,
      });
    } catch (ex) {
      console.error(ex);
    }
  }

  async function upsertFeature(featWrap: MWTypes.FeatureWrapper) {
    try {
      log("SOCKET[upsert-feature]");
      const allowed = await mapAllowed(socket, featWrap.mapId);
      if (!allowed) {
        io.to(socket.id).emit("sync", {
          featureId: featWrap.feature.properties.id,
          layer: featWrap.layer,
          error: "Access denied",
        });
        return;
      }
      setFeatureChangeAuthor(socket, featWrap);
      if (layerSavable(featWrap.layer)) {
        MapCtrl.createLayerIfNotExists(
          featWrap.mapId,
          featWrap.layer,
          socket.client.user
        ); // Don't use await to no delay execution
        gju.upsertFeature(featWrap.mapId, featWrap.layer.id, featWrap.feature);

        setModifiedBy(featWrap.mapId, socket.client.user);
      }
      socket.to(featWrap.mapId).emit("upsert-feature", featWrap);
      io.to(socket.id).emit("sync", {
        featureId: featWrap.feature.properties.id,
      });
    } catch (ex) {
      console.error(ex);
    }
  }
  

  async function setJoinedUsersCount(mapId: string) {
    log("APP[setJoinedUsersCount]");
    io.of("/")
      .in(mapId)
      .clients((error, clients) => {
        if (error) throw error;
        MapModel.findByIdAndUpdate(mapId, {
          usersJoined: clients.length,
        }).exec();
      });
  }

  function featureProcessing(
    featWrap: MWTypes.FeatureWrapper | MWTypes.FeatureChangeWrapper,
    action: string
  ): void {
    createQueueForFeature(featWrap.featureId);
    return featureQueues[featWrap.featureId].q.push(async function (cb) {
      const queueId = Math.random();
      log(
        "APP[writeFeatureQueue item]",
        { queueId, featureId: featWrap.featureId },
        "start"
      );
      switch (action) {
        case featureActions.upsert:
          await upsertFeature(featWrap as MWTypes.FeatureWrapper);
          break;
        case featureActions.change:
          await changeFeature(featWrap as MWTypes.FeatureChangeWrapper);
          break;
        case featureActions.restore:
          log("APP[restore-feature]");
          await deleteOrRestore(featWrap as MWTypes.FeatureWrapper);
          break;
        case featureActions.delete:
          log("APP[delete-feature]");
          await deleteOrRestore(featWrap as MWTypes.FeatureWrapper, true);
          break;
        default:
          break;
      }
      log("APP[writeFeatureQueue item]", { queueId }, "end");
      cb(null);
    });
  }

  async function createQueueForFeature(featureId: string | number) {
    if (featureQueues[featureId]) {
      return;
    }
    featureQueues[featureId] = { q: queue({ results: [], concurrency: 1 }) };
    featureQueues[featureId].q.autostart = true;
    log("APP[createQueueForFeature new queue item]", {
      featureQueue: featureId,
    });
    featureQueues[featureId].q.on("end", function () {
      log("APP[createQueueForFeature delete queue item]", {
        featureQueue: featureId,
      });
      delete featureQueues[featureId];
    });
  }

  async function mapAllowed(socket, mapId?: any) {
    // log('APP[mapAllowed]');
    if (mapId === undefined) {
      return userSessions[socket.id];
    }
    const email = socket.client.user;
    const role = socket.client.role;
    const userRights = await MapPrivilegeCtrl.getSimplified(email, mapId);
    return role === Role.admin || userRights === Right.write;
  }

  function layerSavable(layer: { name: string; id: string }) {
    try {
      if (layer == undefined) {
        console.error("Layer undefined. Exiting deletion");
        return false;
      }
      return !["scratch", "cursor"].includes(layer.name);
    } catch (ex) {
      console.error(ex);
      return false;
    }
  }

  async function deleteOrRestore(
    featWrap: MWTypes.FeatureWrapper,
    featureDeletion?: boolean
  ) {
    const deletion = featureDeletion ? featureDeletion : false;
    try {
      const allowed = await mapAllowed(socket, featWrap.mapId);
      if (!allowed) {
        io.to(socket.id).emit("sync", {
          featureId: featWrap.featureId,
          error: "Access denied",
        });
        return;
      }
      if (layerSavable(featWrap.layer)) {
        gju.deleteOrRestoreFeature(featWrap.featureId, deletion);
        setModifiedBy(featWrap.mapId, socket.client.email);
      }
      setFeatureChangeAuthor(socket, featWrap);
      if (deletion) {
        socket.to(featWrap.mapId).emit("delete-feature", featWrap);
      } else {
        const feature = await GeoJSONFeatureModel.findById(featWrap.featureId);
        featWrap.feature = feature;
        featWrap.type = "restore";
        socket.to(featWrap.mapId).emit("restore-feature", featWrap); //To others
        socket.emit("restore-feature", featWrap); //To originator
      }

      io.to(socket.id).emit("sync", {
        featureId: featWrap.featureId,
        layer: featWrap.layer,
      });
    } catch (ex) {
      console.error(ex);
    }
  }
  socket.on("update-cursor", async (cursorInfo: MWTypes.CursorInfo) => {
    //log('APP[update-cursor]');
    try {
      cursorInfo.i = socket.id;
      const allowed = await mapAllowed(socket);
      if (!allowed) {
        io.to(socket.id).emit("sync", {
          featureId: "cursor",
          layer: "cursor",
          error: "Not authorized",
        });
        return;
      }
      cursorInfo.c = userSessions[socket.id].color;
      cursorInfo.u = userSessions[socket.id].user;
      cursorInfo.un = userSessions[socket.id].username;
      socket
        .to(userSessions[socket.id].mapId)
        .emit("update-cursor", cursorInfo); // test
    } catch (ex) {
      console.error(ex);
    }
  });

  socket.on("disconnect", () => {
    log("SOCKET[disconnect]", { "socket.id": socket.id });
    // Remove cursor
    let cursorIdx = cursors.findIndex((c) => c.id === socket.id);
    if (cursorIdx > -1) {
      cursors.splice(cursorIdx, 1);
    }
    if (userSessions[socket.id]) {
      io.to(userSessions[socket.id].mapId).emit("remove-cursor", socket.id);

      // Delete socket info
      delete userSessions[socket.id];
    }

    // Remove stale maps
    let userMaps = Object.keys(userSessions).map(
      (userSessionId) => userSessions[userSessionId].mapId
    );
    Object.keys(maps)
      .filter((map) => userMaps.indexOf(map) === -1)
      .forEach((staleMap) => {
        log("APP[stale map removal]", { staleMap });
        setJoinedUsersCount(staleMap);
        delete maps[staleMap];
      });
  });
});

http.listen(process.env.PORT || config.serverPort, (err) => {
  if (err) {
    return console.error(err);
  }
  return console.log(
    `Map Whiteboard Server is running and listening for requests on ${config.serverPort}`
  );
});

function setFeatureChangeAuthor(socket: any, featWrap: {user?: string}) {
  const userSession = userSessions[socket.id];
  featWrap.user = userSession.username || userSession.user;
}
