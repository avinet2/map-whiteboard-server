// Connected sockets
export const userSessions: {
    [id: string]: {
      color: string;
      mapId: string;
      user: string;
      username: string;
    };
  } = {};