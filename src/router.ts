import { MapUploadCtrl } from "./controllers/MapUploadCtrl";
import path = require("path");
import { MapCtrl } from "./controllers/MapCtrl";
import { AuthCtrl } from "./controllers/AuthCtrl";
import { sessionMiddleware } from "./middlewares/session";
import { MapPrivilegeCtrl } from "./controllers/MapPrivilegeCtrl";
import { UserCtrl } from "./controllers/UserCtrl";
import config from "./config";
import mapUploader from "./utils/mapUploader";
import imageUploader from "./utils/imageUploader";
import { check } from "express-validator";
import { verifyAuth } from "./middlewares/verify-auth";

const express = require("express");
const cors = require("cors");
const bodyParser = express.json({ strict: false, type: "*/*", limit: "50mb" });

export class Router {
  static configure(app) {
    app.use(
      cors({
        credentials: true,
        origin: function (origin, callback) {
          if (config.serverUrl == origin) {
            callback(null, true);
          } else if (
            !origin ||
            (origin &&
              (origin.indexOf("http://localhost") > -1 ||
                origin.indexOf("vscode-webview://") > -1))
          ) {
            callback(null, true);
          } else {
            callback(new Error(origin + " Not allowed by CORS"));
          }
        },
      })
    );
    app.use((req, res, next) => {
      if(req.headers?.authorization?.includes("Bearer") && !(req.path == '/session' && req.method == 'DELETE')) {
        //Save resources and dont even start session middleware if JWT is used
        next()
      } else {
        //Must use middleware to have access to destroy method to remove session from db on signout. Sessions are also a fallback when JWT is not present
        sessionMiddleware(req, res, next);
      }
    });
    app.use(
      "/public",
      express.static(path.resolve(path.join(config.uploadDir, "..")))
    );
    app.use(express.json());
    app.post("/", (req, res, next) => {
      res.status(200).send("Map whiteboard server is online and running");
    });

    const mapPath = "/maps/:mapId";
    const layerPath = `${mapPath}/layers/:layerId`;
    // Requires auth
    app.all("/session/authenticate", bodyParser, AuthCtrl.authenticate); //Strangely application/json contextType requests don't pass preflight checks and don't even get sent to server, so sending annotated as plain text data, but still json inside
    app.post(
      "/users",
      bodyParser,
      [
        check("username")
          .not()
          .isEmpty()
          .withMessage("Username must be set"),
        check("email", "Email is required").not().isEmpty(),
        check("password", "Password should be at least 3 characters long")
          .not()
          .isEmpty()
          .isLength({ min: 3 }),
      ],
      UserCtrl.register
    );
    app.use(verifyAuth);//All routes after this require req.session to be set either from JWT, sessionID GET param or Cookie
    app.get("/maps/trash", MapCtrl.listTrash);
    app.get("/maps/shared", MapCtrl.listShared);
    app.delete("/maps/trash/:mapId", MapUploadCtrl.deletePermanently);
    app.put("/maps/trash/:mapId", MapCtrl.restoreFromTrash);
    app.post(`${mapPath}/privileges`, bodyParser, MapPrivilegeCtrl.grant);
    app.put(`${mapPath}/privileges`, bodyParser, MapPrivilegeCtrl.update);
    app.post(`${mapPath}/layers`, mapUploader, MapCtrl.addLayerFromJson);
    app.delete(`${layerPath}`, MapCtrl.removeLayer);
    app.put(`${mapPath}/layers/order`, bodyParser, MapCtrl.updateLayerOrder);
    app.put(`${layerPath}`, bodyParser, MapCtrl.updateLayer);
    app.post(`${layerPath}/style`, mapUploader, MapCtrl.styleLayer);
    app.get(`${mapPath}/layers`, MapCtrl.listEditableLayers);
    app.get(`${mapPath}/privileges`, MapPrivilegeCtrl.list);
    app.delete(`${mapPath}/privileges/:privilegeId`, MapPrivilegeCtrl.revoke);
    app.get(`${layerPath}/features`, MapCtrl.getLayerFeatures);
    app.post(`${layerPath}/features`, bodyParser, MapCtrl.publishLayerFeatures);
    app.delete(`${layerPath}/clear`, MapCtrl.clearLayer);
    app.get("/session", AuthCtrl.getSession);
    app.delete("/session", AuthCtrl.signOut);
    app.delete(`${mapPath}`, MapCtrl.moveToTrash);
    app.put(`${mapPath}/metadata`, bodyParser, MapCtrl.updateMetadata);
    app.get(`${mapPath}/metadata`, MapCtrl.getMetadata);
    app.get(`${mapPath}`, MapCtrl.download);
    app.put(`${mapPath}`, bodyParser, MapCtrl.updateMap);
    app.post("/maps", mapUploader, MapUploadCtrl.create);
    app.get("/maps", MapCtrl.list);
    app.get("/users", UserCtrl.list);
    app.put("/users", bodyParser, UserCtrl.changeRole);
    app.get("/users/find/:query", UserCtrl.find);
    app.post("/users/profile/image", imageUploader, UserCtrl.uploadProfileImg);
    app.delete("/users/:userId", UserCtrl.delete);
  }
}
