const DB_SERVER = process.env.DB_SERVER || "127.0.0.1";
const SERVER_URL = process.env.SERVER_URL || "https://whiteboard-demo.lv";
const IMAGE_URL_BASE =
  process.env.IMAGE_URL_BASE || "http://localhost:3101/images/";
let ALLOWED_AUTH_PROVIDERS;
if (process.env.ALLOWED_AUTH_PROVIDERS) {
  ALLOWED_AUTH_PROVIDERS = process.env.ALLOWED_AUTH_PROVIDERS.split(
    ","
  ).map((p) => p.trim());
} else {
  ALLOWED_AUTH_PROVIDERS = ["FACEBOOK", "GOOGLE", '']; // Local mongodb, FACEBOOK, GOOGLE allowed by default
}
const ALLOWED_ENDPOINTS = process.env.ALLOWED_ENDPOINTS ||[
  "https://dih.bosc.lv/modeller/admin/api/",
  "https://local.polirural-mb.lv/admin/api/",
];
export default {
  sessionExpiresSeconds: parseFloat(process.env.SESSION_EXPIRES_SECONDS) || 14 * 24 * 60 * 60,
  allowedAuthProviders: ALLOWED_AUTH_PROVIDERS,
  allowedEndpoints: ALLOWED_ENDPOINTS,
  serverPort: process.env.SERVER_PORT || 3101,
  serverUrl: SERVER_URL,
  uploadDir: "./public/images/",
  imageUrlBase: IMAGE_URL_BASE,
  mongoConnection: `mongodb://${DB_SERVER}/map-whiteboard`,
  sessionSecret: process.env.SESSION_SECRET || "e79e92c6-4522-4940-958d-11d56dasdb69f5fb",
};
