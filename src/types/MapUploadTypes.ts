import mongoose from 'mongoose';

export interface UploadMap {
  filename: string;
}

export interface IUploadMap extends mongoose.Document, UploadMap {}

var UploadMapSchema = new mongoose.Schema({
  url: { type: String, required: true },
  filename: { type: String, required: true },
});

export const UploadMapModel = mongoose.model<IUploadMap>(
  "UploadFile",
  UploadMapSchema,
  "uploadfiles"
);

export default UploadMapModel;
