import { Provider } from "../auth-providers/auth-provider.interface";

export type Credentials = {
  provider: Provider;
  email?: string
  username?: string;
  password?: string;
  role?: string;
  sessionID?: string;
  socialToken?: string;
  options?: { url: string };
};
