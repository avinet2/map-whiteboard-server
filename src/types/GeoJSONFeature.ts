import mongoose from 'mongoose';
import { Map } from "./Map";

export interface GeoJSONFeature {
  _id: any;
  _map: Map | string;
  _layer: string;
  type: "Feature";
  geometry: {
    type: "Point" | "LineString" | "Polygon";
    coordinates: Array<any>;
  };
  properties: {
    id: any;
  };
}

export const GeoJSONFeatureSchema = new mongoose.Schema({
  _id: { type: String, required: true },
  _map: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "map",
    required: true,
  },
  _layer: { type: String, required: true },
  type: {
    type: String,
    required: true,
    validate: (t) => t === "Feature",
  },
  geometry: {
    type: { type: String, required: true },
    coordinates: { type: Array, required: true },
  },
  properties: {},
  deleted: { type: Boolean, required: false },
});

export interface IGeoJSONFeature extends GeoJSONFeature {}

export const GeoJSONFeatureModel = mongoose.model<IGeoJSONFeature>(
  "feature",
  GeoJSONFeatureSchema,
  "features"
);

export default GeoJSONFeatureModel;
