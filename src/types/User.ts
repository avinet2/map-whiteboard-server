import mongoose from "mongoose";
import uniqueValidator = require("mongoose-unique-validator");
import crypto = require("crypto");

export enum Role {
  admin = "Admin",
  moderator = "Moderator",
  user = "User",
}
export interface User {
  _id: any;
  username: String;
  email: String;
  role: Role;
  avatar?: String;
  validPassword(password);
  setPassword(password);
}

export const UserSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      lowercase: true,
      unique: true,
      required: [true, "can't be blank"],
      match: [/^[a-zA-Z0-9]+$/, "is invalid"],
      index: true,
    },
    email: {
      type: String,
      lowercase: true,
      unique: true,
      required: [true, "can't be blank"],
      match: [/\S+@\S+\.\S+/, "is invalid"],
      index: true,
    },
    role: { type: String, default: Role.user }, //admin, viewer
    hash: String,
    salt: String,
    avatar: String,
  },
  { timestamps: true }
);

UserSchema.plugin(uniqueValidator, { message: "is already taken." });

export interface IUser extends User {}

UserSchema.methods.setPassword = function (password) {
  this.salt = crypto.randomBytes(16).toString("hex");
  this.hash = crypto
    .pbkdf2Sync(password, this.salt, 10000, 512, "sha512")
    .toString("hex");
};

UserSchema.methods.validPassword = function (password) {
  try {
    const hash = crypto
      .pbkdf2Sync(password, this.salt, 10000, 512, "sha512")
      .toString("hex");
    return this.hash === hash;
  } catch (e) {
    return false;
  }
};

export const UserModel = mongoose.model<IUser>("User", UserSchema);

export default UserModel;
