import mongoose from 'mongoose';
import { UploadMap } from "./MapUploadTypes";

export interface Map {
  _id: any;
  title: string;
  abstract?: string;
  file: UploadMap | string;
  defaultRights: string;
  extent: Array<number>;
  author: string;
  usersJoined: number;
  layers: Array<any>;
}

export const MapSchema = new mongoose.Schema(
  {
    _id: { type: mongoose.Schema.Types.ObjectId, required: true, auto: true },
    title: { type: String, required: true },
    abstract: { type: String, required: false },
    file: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "UploadFile",
    },
    defaultRights: { type: String, required: false },
    extent: { type: Array, required: true },
    author: { type: String, required: false },
    deleted: { type: Boolean, required: false },
    lastModifiedBy: { type: String, required: false },
    usersJoined: { type: Number, required: false },
    layers: { type: Array, required: false },
  },
  { timestamps: true }
);

export interface IMap extends Map {
  deleted: boolean;
  file: UploadMap;
}

export const MapModel = mongoose.model<IMap>("map", MapSchema, "maps");

export default MapModel;
