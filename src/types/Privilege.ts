import mongoose from 'mongoose';

export enum Right {
  own = "own",
  read = "read",
  write = "write",
  restrict = "restrict",
}
export interface Privilege {
  _id: any;
  map: string;
  user: string;
  right: Right; //own, edit, view
  lastAccess: Date;
}

export const PrivilegeSchema = new mongoose.Schema({
  _id: { type: mongoose.Schema.Types.ObjectId, required: true, auto: true },
  map: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Map",
  },
  user: { type: String, required: true },
  right: { type: String, required: true }, //own, write, read
  lastAccess: { type: Date, required: false },
});

export interface IPrivilege extends Privilege {}

export const PrivilegeModel = mongoose.model<IPrivilege>(
  "privilege",
  PrivilegeSchema
);

export default PrivilegeModel;
