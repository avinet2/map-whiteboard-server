import MongoStore from "connect-mongo";
import config from "../config";
const session = require("express-session");

export const store = MongoStore.create({
  mongoUrl: config.mongoConnection,
  ttl: config.sessionExpiresSeconds,
});
export const sessionMiddleware = session({
  name: "connect-mapWb.sid",
  store,
  secret: config.sessionSecret,
  cookie: {
    maxAge: config.sessionExpiresSeconds * 1000,
  },
  resave: true,
  saveUninitialized: true,
});
