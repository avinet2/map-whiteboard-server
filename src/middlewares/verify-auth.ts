import { log } from "../utils/logs";
import { sendUnauthorized } from "../utils/responses";
import { fillGetSession, fillJWTSession, processBasicAuth } from "./auth";

export function verifyAuth(req, res, next) {
  log("REST", req.path);
  if (req.headers?.authorization) {
    if (req.headers.authorization.indexOf("Basic") > -1) {
      return processBasicAuth(req, res, next);
    } else if (req.headers.authorization.indexOf("Bearer") > -1) {
      return fillJWTSession(req, res, next);
    } else {
      return sendUnauthorized(res);
    }
  } else if (req.session?.user) {
    //Session from cookie already exists. Ignore rest.
    next();
    return;
  }
  // Find session by query GET parameter for swagger where cookies are not passed
  if (req.query.session) {
    fillGetSession(req, res, next);
  } else {
    sendUnauthorized(res);
  }
}
