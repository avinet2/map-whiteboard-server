import { Provider } from "../auth-providers/auth-provider.interface";
import { AuthCtrl } from "../controllers/AuthCtrl";
import { sendAccessDenied, sendUnauthorized } from "../utils/responses";

const jwt = require("jsonwebtoken");
import config from "./../config";
export function fillJWTSession(req, res, next) {
  try {
    const token = req.headers.authorization.split(" ")[1];
    if (token == null) {
      return sendAccessDenied(res);
    }
    return jwt.verify(token, config.sessionSecret, (err, data) => {
      if (err) {
        req.session = undefined;
        return sendUnauthorized(res);
      } else {
        if (!req.session) {
          req.session = {};
        }
        Object.assign(req.session, data);
        req.sessionID = data.sessionID;
      }
      next();
    });
  } catch (error) {
    sendUnauthorized(res);
  }
}

export async function processBasicAuth(req, res, next) {
  const decoded = new Buffer(
    req.headers.authorization.split(" ")[1],
    "base64"
  ).toString();
  const credentials = decoded.split(":");
  if (credentials.length == 2) {
    const result = AuthCtrl.authenticateContext(
      req,
      {
        username: credentials[0],
        password: credentials[1],
        provider: Provider.WhiteBoardDbAuth,
      }
    );
    next();
  }
}

export function fillGetSession(req, res, next) {
  req.sessionID = req.query.session;
  req.sessionStore.get(req.query.session, function (err, sess) {
    // This attaches the session to the req.
    if (sess) {
      req.sessionStore.createSession(req, sess);
    }
    next();
  });
}
