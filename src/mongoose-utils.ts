import mongoose from 'mongoose';
import config from "./config";

export class MongoDBUtils {
  static _db = null;
  static _connected = null;

  static staticConstructor() {
    let cstr2 = config.mongoConnection;
    mongoose.connect(cstr2, {
    });

    this._db = mongoose.connection;
    this._db.on(
      "error",
      console.error.bind(
        console,
        "Map whiteboard server failed to connect to database:"
      )
    );
    this._db.once("open", function () {
      this._connected = true;
      console.log("Map whiteboard server is connected to MongoDB");
    });
  }

  static getObjectId() {
    return new mongoose.Types.ObjectId();
  }

  static success(data: any) {
    return {
      success: true,
      data: data,
    };
  }

  static error(message: any) {
    const msg = message?.message || message;
    return {
      success: false,
      message: msg,
    };
  }
}

MongoDBUtils.staticConstructor();

export default MongoDBUtils;
