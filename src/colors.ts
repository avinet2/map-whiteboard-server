import { log } from "./utils/logs";

export module Colors {
  const IdentityColors = [
    "#a6cee3",
    "#1f78b4",
    "#b2df8a",
    "#33a02c",
    "#fb9a99",
    "#e31a1c",
    "#fdbf6f",
    "#ff7f00",
    "#cab2d6",
    "#6a3d9a",
    "#ffff99",
    "#b15928",
  ];

  var usedColors = [];

  export const getColor = function () {
    log('COLORS[getColor]');
    if (usedColors.length === 0) {
      usedColors = IdentityColors.slice(0);
    }
    return usedColors.pop();
  };

  export const makeRandomColor = function (): string {
    log('COLORS[makeRandomColor]');
    var c = "";
    while (c.length < 7) {
      c += Math.random().toString(16).substr(-6).substr(-1);
    }
    return "#" + c;
  };
}
