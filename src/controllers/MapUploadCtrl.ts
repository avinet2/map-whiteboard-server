import UploadMapModel, { IUploadMap } from "../types/MapUploadTypes";
import path = require("path");
import fs = require("fs");
import PrivilegeModel, { Right } from "../types/Privilege";
import MapModel, { IMap } from "../types/Map";
import { MapPrivilegeCtrl } from "./MapPrivilegeCtrl";
import gju from "./../gjutils";
import {
  sendAccessDenied,
  sendBadRequest,
  sendInternalError,
  sendNotFound,
  sendRequestCompleted,
  sendSuccessData,
} from "../utils/responses";
import { Role } from "../types/User";
import { log } from "../utils/logs";
import { distributeMapInteraction } from "./MapCtrl";
import config from "../config";
const util = require("util");
const readFile = util.promisify(fs.readFile);

export class MapUploadCtrl {
  static async create(req, res) {
    log("UPLOAD[create]");
    //<input type="file" name="file">
    if (!req.file) {
      return sendBadRequest(res, "No files were uploaded");
    }

    if (!req.session?.user) {
      return sendAccessDenied(res);
    }

    let file = req.file;

    const newFullFileName = config.uploadDir + file.filename;
    const newFullUrl = config.imageUrlBase + file.filename;

    // Use the mv() method to place the file somewhere on your server

    try {
      const uploadFile: IUploadMap = await UploadMapModel.create({
        url: newFullUrl,
        filename: newFullFileName,
      });

      const compFilename = path.resolve(newFullFileName);
      const compData = await readFile(compFilename, "utf8");
      const compObj = JSON.parse(compData);
      const title = compObj.title;
      const extent = compObj.extent;
      const abstract = compObj.abstract;
      const author =
        req.session.username ||
        (await MapPrivilegeCtrl.getValidUser(req.session.user));
      const defaultRights =
        req.session.authProvider == "ZeroAuthProvider" ? "write" : "restrict";
      const layers = compObj.layers;
      const map: IMap = await MapModel.create({
        title,
        abstract,
        file: uploadFile._id,
        defaultRights,
        extent,
        author,
        lastModifiedBy: author,
        layers,
      });

      await PrivilegeModel.create({
        map: map._id,
        user: req.session.user,
        right: Right.own,
        lastAccess: new Date(),
      });

      for (const layer of layers) {
        if (layer.features?.features?.length > 0) {
          gju.insertFeatures(
            map._id.toString(),
            layer.metadata.id,
            layer.features,
            compObj.projection
          );
        }
      }

      sendSuccessData(res, map);
    } catch (ex) {
      sendInternalError(res, ex);
    }
  }

  static async deletePermanently(req, res) {
    log("UPLOAD[deletePermanently]");
    try {
      const mapId = req.params.mapId;
      if (mapId !== undefined) {
        const compItem = await MapModel.findById(mapId).exec();
        const fileId = compItem.file.toString();
        const rights = await MapPrivilegeCtrl.get(req.session.user, mapId);
        if (
          req.session.role === Role.admin ||
          (rights === Right.own && fileId !== undefined)
        ) {
          await MapModel.findByIdAndDelete(mapId).exec();
          await PrivilegeModel.deleteMany({ map: mapId }).exec();
          const uploadFile = await UploadMapModel.findById(
            fileId).exec();
          if (uploadFile) {
            try {
              fs.unlink(uploadFile.filename, () => {
                distributeMapInteraction({
                  mapId,
                  reason: "map-remove"
                });
                return sendRequestCompleted(res);
              });
            } catch (ex) {
              sendInternalError(res, ex);
            } finally {
              await uploadFile.delete();
            }
          }
        } else {
          return sendAccessDenied(res);
        }
      }
    } catch (ex) {
      sendInternalError(res, ex);
    }
  }
}
