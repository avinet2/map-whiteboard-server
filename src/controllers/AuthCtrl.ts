import { KeystoneGraphqlProvider } from "../auth-providers/keystone-graphql";
import { ZeroAuthProvider } from "../auth-providers/zero-auth";
import {
  AuthResult,
  AuthUserData,
  Provider,
} from "../auth-providers/auth-provider.interface";
import { Credentials } from "../types/Credentials";
import { AuthContext } from "../auth-providers/auth-context";
import { store } from "../middlewares/session";
import { WhiteBoardDbAuthProvider } from "../auth-providers/whiteboard-db";
import config from "../config";
import { log } from "../utils/logs";
import { SocialAuthProvider } from "../auth-providers/social-provider";
const jwt = require("jsonwebtoken");
import { Role } from "../types/User";
import {
  sendAccessDenied,
  sendBadRequest,
  sendInternalError,
  sendSuccessData,
} from "../utils/responses";
import { userSessions } from "../volatile-data";

const allowedProviders = config.allowedAuthProviders;

export class AuthCtrl {
  static getSession(req, res) {
    log("AUTHCTRL[getSession]");
    if (req.sessionID) {
      sendSuccessData(res, {
        sessionID: req.sessionID,
        sessionInfo: req.session,
      });
    } else {
      sendAccessDenied(res);
    }
  }

  static async authenticate(req, res) {
    log("AUTHCTRL[authenticate]");
    //res.header("Access-Control-Allow-Origin", config.serverUrl); //TODO: expose this in config //  app.use(cors({ already does it
    res.header("Access-Control-Allow-Credentials", "true");
    const result = await AuthCtrl.authenticateContext(req, req.body);
    if (result?.error) {
      sendBadRequest(res, result.error);
    } else if (result.success) {
      let jwtToken;
      try {
        jwtToken = jwt.sign(result.sessionData, config.sessionSecret, {
          expiresIn: config.sessionExpiresSeconds,
        });
      } catch (ex) {
        console.error("Could not generate JWT");
      }
      sendSuccessData(res, {
        success: true,
        status: "Access granted",
        sessionID: req.sessionID,
        jwt: jwtToken,
        sessionInfo: req.session,
      });
    } else {
      sendAccessDenied(res);
    }
  }

  static async signOut(req, res) {
    log("AUTHCTRL[signOut]");
    try {
      if (req.session.destroy) {
        req.session.destroy();
      }
      sendSuccessData(res, "Session destroyed");
    } catch (ex) {
      sendInternalError(res, ex);
    }
  }

  static async getSessionPromise(sessionID: string): Promise<any> {
    return new Promise((resolve, reject) => {
      store.get(sessionID, function (err, sess) {
        resolve(sess);
      });
    });
  }

  static async authenticateContext(
    context: AuthContext,
    data: Credentials
  ): Promise<{
    success: boolean;
    error?: any;
    sessionData?: {
      sessionID: string;
      user: String;
      username: string | String;
      userId: any;
      role: any;
      avatar: any;
      socialToken: string;
      authProvider: string;
    };
  }> {
    log("Auth[authenticate]");
    if (
      allowedProviders.indexOf(data.provider) > -1 ||
      data.sessionID != undefined
    ) {
      if (data.sessionID) {
        const sess = await AuthCtrl.getSessionPromise(data.sessionID);
        if (sess) {
          //Set for socket
          if (context.client != undefined) {
            context.client.user = sess.user;
            context.client.username = sess.username;
            context.client.role = sess.role;
            userSessions[context.id].username = sess.username;
          }
          //Set for req
          if (context.session != undefined) {
            context.session.user = sess.user;
          }
          return { success: true };
        } else {
          return;
        }
      }
      let result: AuthResult;
      switch (data.provider) {
        case Provider.KeystoneGraphql:
          if (data.options == undefined) {
            return {
              success: false,
              error: `Authentification failed because payload didn't contain options attribute, which is needed for KeystoneGraphql provider`,
            };
          }
          const keystoneProvider = new KeystoneGraphqlProvider(data.options);
          result = await keystoneProvider.authenticate(context, data);
          break;
        case Provider.Liferay:
          //TODO Implement liferay auth
          break;
        case Provider.ZeroAuth:
          const zeroProvider = new ZeroAuthProvider(data.options);
          //This branch doesn't execute if we don't specify 'zero' in allowedProviders or client sends a non-empty provider
          result = await zeroProvider.authenticate(context, data);
        case Provider.Facebook:
        case Provider.Google:
          const socialProvider = new SocialAuthProvider(data.options);
          result = await socialProvider.authenticate(context, data);
          break;
        case Provider.WhiteBoardDbAuth:
        default:
          const whiteboardDbProvider = new WhiteBoardDbAuthProvider(
            data.options
          );
          result = await whiteboardDbProvider.authenticate(context, data);
      }
      if (result?.user) {
        const sessionData = AuthCtrl.createSessionData(
          context,
          result.user,
          data
        );
        AuthCtrl.setSession(context, sessionData);
        return { success: true, sessionData };
      } else if (result?.error) {
        return { success: false, error: result.error };
      } else {
        return { success: false };
      }
    } else {
      return { success: false };
    }
  }

  static postAuthenticate(socket: AuthContext, data: Credentials) {
    log("Auth[postAuthenticate]");
    if (allowedProviders.indexOf(data.provider) > -1) {
      switch (data.provider) {
        case Provider.KeystoneGraphql:
          new KeystoneGraphqlProvider(data.options).postAuthenticate(
            socket,
            data
          );
          break;
        case Provider.Liferay:
          //TODO Implement liferay postAuth
          break;
        default:
          //This branch doesn't execute if we don't specify '' in allowedProviders or client sends a non-empty provider
          new ZeroAuthProvider(data.options).postAuthenticate(socket, data);
      }
    }
  }

  static disconnect(socket) {
    log("Auth[disconnect]");
    console.log(socket.id + " disconnected by authentication system");
  }

  /**
   * Stores info about user either in session or on socket object
   * @param context
   * @param sessionData
   */
  static setSession(context: AuthContext, sessionData: any) {
    //For file/db based express sessions
    if (context.session != undefined) {
      Object.assign(context.session, sessionData);
    }
    //For sockets
    if (context.client != undefined) {
      Object.assign(context.client, sessionData);
    }
    console.log(
      `User ${sessionData.username} authenticated with ${sessionData.authProvider}`
    );
  }

  static createSessionData(
    context: AuthContext,
    user: AuthUserData,
    data: Credentials
  ): {
    sessionID: string;
    user: String;
    username: string | String;
    userId: any;
    role: any;
    avatar: any;
    socialToken: string;
    authProvider: string;
  } {
    const authProvider = data.provider
      ? data.provider + "Provider"
      : "WhiteBoardDbAuthProvider";
    return {
      sessionID: context.sessionID,
      user: user.email,
      username: data.username ?? user.username,
      userId: user._id,
      role: user.role || Role.user,
      avatar: user.avatar,
      socialToken: data.socialToken,
      authProvider,
    };
  }
}
