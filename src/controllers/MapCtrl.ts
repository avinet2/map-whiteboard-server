import { Role } from "../types/User";
import path = require("path");
import MapModel from "../types/Map";
import PrivilegeModel, { Right } from "../types/Privilege";
import fs = require("fs");
import GeoJSONFeatureModel from "../types/GeoJSONFeature";
import uid from "uid-safe";
import { MapPrivilegeCtrl } from "./MapPrivilegeCtrl";
import {
  sendAccessDenied,
  sendBadRequest,
  sendInternalError,
  sendNotFound,
  sendRequestCompleted,
  sendSuccessData,
} from "../utils/responses";
import { log } from "../utils/logs";
import gju from "./../gjutils";
import config from "../config";
const util = require("util");
const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);
const queue = require("queue");
var mapQueues: {
  [mapId: string]: {
    q: any; //queue object
  };
} = {};
type CompositionWrapper = {
  filename: string;
  object: any;
};

export function distributeMapInteraction(data: {
  mapId: string;
  reason: "map-remove" | "map-update";
  definition?: any;
}) {
  log("MAPCTRL[distributeMapInteraction]");
  MapCtrl.io.to(data.mapId).emit("map-interaction", data);
}

function distributeMapChange(data: {
  mapId: string;
  reason:
    | "layer-add"
    | "layer-update"
    | "layer-style"
    | "layer-order-change"
    | "layer-remove";
  layerId?: string;
  definition: any;
}) {
  log("MAPCTRL[distributeMapChange]");
  MapCtrl.io.to(data.mapId).emit("map-definition-change", data);
}

type MapChangeMetadata = {
  mapId: string;
  email: string;
};

type LayerDescriptor = {
  name: string;
  id: string;
};

export class MapCtrl {
  static existingLayerCache = {};
  static io;

  private static createQueueForMap(mapId: string) {
    if (mapQueues[mapId]) {
      return;
    }
    mapQueues[mapId] = { q: queue({ results: [], concurrency: 1 }) };
    mapQueues[mapId].q.autostart = true;
    log("MAPCTRL[createQueueForMap new queue item]", { mapQueue: mapId });
    mapQueues[mapId].q.on("end", function () {
      log("MAPCTRL[createQueueForMap delete queue item]", { mapQueue: mapId });
      delete mapQueues[mapId];
    });
  }

  private static readFileQueue(
    mapId: string,
    filename: string //TODO create a separate queue for each mapId
  ): Promise<any> {
    MapCtrl.createQueueForMap(mapId);
    return new Promise((resolve, reject) => {
      return mapQueues[mapId].q.push(async function (cb) {
        log("MAPCTRL[readFileQueue cb]");
        try {
          const compFilename = path.resolve(filename);
          const compData = await readFile(compFilename, "utf8");
          const parsedObj = JSON.parse(compData);
          cb(null, parsedObj);
          resolve(parsedObj);
        } catch (ex) {
          console.error(ex);
          cb(ex);
        }
      });
    });
  }

  private static writeFileQueue(
    composition: CompositionWrapper,
    mapChangeMetadata: MapChangeMetadata
  ): void {
    MapCtrl.createQueueForMap(mapChangeMetadata.mapId);
    return mapQueues[mapChangeMetadata.mapId].q.push(async function (cb) {
      const queueId = Math.random();
      log(
        "MAPCTRL[writeFileQueue item]",
        { queueId, filename: composition.filename },
        "start"
      );
      await writeFile(composition.filename, JSON.stringify(composition.object));
      await MapModel.findByIdAndUpdate(mapChangeMetadata.mapId, {
        lastModifiedBy: mapChangeMetadata.email,
      }).exec();
      log("MAPCTRL[writeFileQueue item]", { queueId }, "end");
      cb(null);
    });
  }

  static async createLayerIfNotExists(
    mapId: any,
    layer: LayerDescriptor,
    email: string
  ) {
    const mapLayerKey = `${mapId}-${layer.id}`;
    if (layer.id == "scratch") return;
    if (MapCtrl.existingLayerCache[mapLayerKey]) return;
    log("MAPCTRL[createLayerIfNotExists]");
    const layers = await MapCtrl.getEditableLayers(mapId);
    const found = layers.filter((looped) => looped.id == layer.id);
    if (found.length == 0) {
      console.log(
        `Layer with name ${layer.name} didn't exist for map ${mapId}. Creating one.`
      );
      return MapCtrl.addLayerObj(
        mapId,
        {
          metadata: { id: layer.id },
          visibility: true,
          opacity: 1,
          title: layer.name,
          className: "Vector",
        },
        email
      );
    } else {
      MapCtrl.existingLayerCache[mapLayerKey] = true;
    }
  }

  static async getLayerFeatures(req, res) {
    log("MAPCTRL[getLayerFeatures]");
    const mapId = req.params.mapId;
    if (await MapPrivilegeCtrl.canIRead(req, mapId)) {
      const features = await GeoJSONFeatureModel.find({
        _map: mapId,
        _layer: req.params.layerId,
        deleted: { $ne: true },
      }).exec();
      if (features) {
        return res.status(200).json({ type: "FeatureCollection", features }); //Not using {success: true ..} structure to be geojson compatible
      } else {
        const layers = await MapCtrl.getEditableLayers(mapId);
        const foundLayer = layers.find(
          (layer) => layer.id == req.params.layerId
        );
        if (foundLayer) {
          return res
            .status(200)
            .json({ type: "FeatureCollection", features: [] });
        } else {
          return sendNotFound(res, "Layer not found");
        }
      }
    } else {
      sendAccessDenied(res);
    }
  }

  static async publishLayerFeatures(req, res) {
    try {
      log("MAPCTRL[publishLayerFeatures]");
      const mapId = req.params.mapId;
      const layerId = req.params.layerId;
      const body = JSON.parse(JSON.stringify(req.body));
      const features = body.features;
      if (await MapPrivilegeCtrl.canIWrite(req, mapId)) {
        const def = await MapCtrl.readCompositionObj(mapId);
        const layers = await MapCtrl.getEditableLayers(mapId);
        const foundLayer = layers.find((layer) => layer.id == layerId);
        if (foundLayer) {
          if (features?.features?.length > 0) {
            gju.insertFeatures(mapId, layerId, features, def.object.projection);
            return sendRequestCompleted(res);
          } else {
            sendBadRequest(res, "No features were added");
          }
        } else {
          return sendNotFound(res, "Layer not found");
        }
      } else {
        sendAccessDenied(res);
      }
    } catch (ex) {
      sendInternalError(res, ex);
    }
  }

  static async clearLayer(req, res) {
    const mapId = req.params.mapId;
    const email = req.session.user;
    log("MAPCTRL[clearLayer]", { mapId });
    if (await MapPrivilegeCtrl.canIWrite(req, mapId)) {
      //user can clear layer only if he has write rights
      const layers = await MapCtrl.getEditableLayers(mapId);
      const foundLayers = layers.filter(
        (layer) => layer.id == req.params.layerId
      );

      switch (foundLayers.length) {
        case 1:
          const result = await GeoJSONFeatureModel.remove({
            _map: mapId,
            _layer: req.params.layerId,
          }).exec();
          await MapModel.findByIdAndUpdate(mapId, {
            lastModifiedBy: email,
          }).exec();
          return sendRequestCompleted(res);
        case 0:
          return sendNotFound(res, "Layer not found");
        default:
          sendBadRequest(res, "Multiple layers with the same id exist");
      }
    } else {
      sendAccessDenied(res);
    }
  }

  static async listEditableLayers(req, res) {
    const mapId = req.params.mapId;
    log("MAPCTRL[listEditableLayers]", { mapId });
    if (await MapPrivilegeCtrl.canIRead(req, mapId)) {
      //can load layers in any case
      try {
        const layers = await MapCtrl.getEditableLayers(mapId);
        sendSuccessData(res, layers);
      } catch (ex) {
        sendInternalError(res, ex);
      }
    } else {
      sendAccessDenied(res);
    }
  }

  static async getEditableLayers(mapId: string): Promise<LayerDescriptor[]> {
    log("MAPCTRL[getEditableLayers]", { mapId });
    const def = await MapCtrl.readCompositionObj(mapId);
    const layers = def.object.layers
      .filter((layer) => layer.className == "Vector")
      .map((layer) => {
        return {
          name: layer.name || layer.title,
          id: layer.metadata.id,
          style: layer.style,
        };
      });
    return layers;
  }

  static async list(req, res) {
    log("MAPCTRL[list]");
    try {
      const filter =
        req.session.role === Role.admin
          ? {}
          : { user: req.session.user, right: Right.own };
      const privileges = await PrivilegeModel.find(filter).exec();
      const mapIdsGranted = privileges.map((p) => p.map.toString());
      const maps = await MapModel.find({
        _id: { $in: mapIdsGranted },
        deleted: { $ne: true },
      }).exec();
      sendSuccessData(res, maps);
    } catch (ex) {
      sendInternalError(res, ex);
    }
  }

  static async download(req, res) {
    const mapId = req.params.mapId;
    log("MAPCTRL[download]", { mapId });
    const compItem = await MapCtrl.readCompositionObj(mapId);
    if (!compItem) {
      return sendNotFound(res, "Composition not found");
    }
    if (await MapPrivilegeCtrl.canIRead(req, mapId)) {
      //can load layers in any case
      try {
        const def = await MapCtrl.readCompositionObj(mapId);
        await PrivilegeModel.findOneAndUpdate(
          {
            map: mapId,
            user: req.session.user,
          },
          { lastAccess: new Date() }
        );
        return res.status(200).sendFile(path.resolve(def.filename));
      } catch (ex) {
        sendNotFound(res, ex);
      }
    } else {
      return sendAccessDenied(res);
    }
  }

  static async addLayerFromJson(req, res) {
    try {
      const email = req.session.user;
      const mapId = req.params.mapId;
      log("MAPCTRL[addLayerFromJson]", { mapId });
      if (!(await MapPrivilegeCtrl.canIWrite(req, mapId))) {
        sendAccessDenied(res);
        return;
      }
      const def = await MapCtrl.readCompositionObj(mapId);
      let file = req.file;
      const layerFilename = config.uploadDir + file.filename;
      const layerData = await readFile(layerFilename, "utf8");
      const layerObj = JSON.parse(layerData);
      if (layerObj.metadata?.id == "scratch") {
        sendInternalError(res, "Scratch layer can't be added to composition");
        return;
      } else {
        if (layerObj.features?.features?.length > 0) {
          /* Example layerObj:  {
              'metadata': {},
              'className': 'Vector',
              'features': {
                'type': 'FeatureCollection',
                'features': [
                  {
                    'type': 'Feature',
                    'geometry': {
                      'type': 'Point',
                      'coordinates': [25.486272576058198, 57.21506027419332],
                    },
                    'properties': null,
                  },
                ],
              },..
              */
          gju.insertFeatures(
            mapId,
            layerObj.metadata.id,
            layerObj.features,
            def.object.projection
          );
        }
      }

      const compositionObj = await MapCtrl.addLayerObj(mapId, layerObj, email);
      distributeMapChange({
        mapId,
        reason: "layer-add",
        layerId: layerObj.metadata.id,
        definition: compositionObj,
      });
      fs.unlink(layerFilename, () => {});
      sendSuccessData(res, layerObj);
    } catch (ex) {
      sendInternalError(res, ex);
    }
  }

  /**
   * Add layer which is a JSON object to map composition file
   * @param mapId {string}
   * @param layerObj {any}
   */
  public static async addLayerObj(mapId: string, layerObj: any, email: string) {
    log("MAPCTRL[addLayerObj]", { mapId });
    const def = await MapCtrl.readCompositionObj(mapId);
    if (layerObj.metadata == undefined) {
      layerObj.metadata = {};
    }
    if (layerObj.metadata.id == undefined) {
      layerObj.metadata.id = uid.sync(18);
    }
    def.object.layers.push(layerObj);

    MapCtrl.writeFileQueue(def, { mapId, email });
    return def.object;
  }

  static async removeLayer(req, res) {
    const mapId = req.params.mapId;
    const email = req.session.user;
    log("MAPCTRL[removeLayer]", { mapId });
    if (await MapPrivilegeCtrl.canIWrite(req, mapId)) {
      try {
        const layerId = req.params.layerId;
        const def = await MapCtrl.readCompositionObj(mapId);
        def.object.layers = def.object.layers.filter(
          (layer) => layer.metadata.id != layerId
        );
        MapCtrl.writeFileQueue(def, { mapId, email });
        sendRequestCompleted(res);
        distributeMapChange({
          mapId,
          reason: "layer-remove",
          layerId: req.params.layerId,
          definition: def.object,
        });
      } catch (ex) {
        sendNotFound(res, "Composition not found");
      }
    } else {
      sendAccessDenied(res);
    }
  }

  static async readCompositionObj(mapId: string): Promise<CompositionWrapper> {
    try {
      log("MAPCTRL[readCompositionObj]", { mapId });
      const compItem = await MapModel.findById(mapId).populate("file").exec();
      if (compItem == null) {
        throw `Could not find composition by ID ${mapId}`;
      }
      const filename = path.resolve(compItem.file.filename);
      const object = await MapCtrl.readFileQueue(mapId, filename);
      const def = { filename, object };
      //Strip features from composition definition to save traffic
      for(let layer of object.layers) {
        if (layer.features !== undefined) {
          delete layer.features;
        }
      }
      return def;
    } catch (ex) {
      console.error(ex);
      return;
    }
  }

  //This is needed for backwards compatibility when features where linked to layer name not ID
  static async addLayerIdsIfNecessary(
    def: CompositionWrapper,
    modifiedBy: MapChangeMetadata
  ): Promise<void> {
    log("MAPCTRL[addLayerIdsIfNecessary]");
    try {
      const withoutIds = def.object.layers.filter(
        (layer) => layer.metadata == undefined || layer.metadata.id == undefined
      );
      if (withoutIds.length > 0) {
        for (const layer of withoutIds) {
          const layerId = uid.sync(18);
          layer.metadata.id = layerId;
          await GeoJSONFeatureModel.updateMany(
            { _layer: layer.title },
            { $set: { _layer: layerId } }
          );
        }
        MapCtrl.writeFileQueue(def, modifiedBy);
      }
    } catch (ex) {
      console.error(ex);
      throw ex;
    }
  }

  static async updateLayer(req, res) {
    const email = req.session.user;
    const mapId = req.params.mapId;
    log("MAPCTRL[addLayerIdsIfNecessary]", { mapId });
    if (await MapPrivilegeCtrl.canIWrite(req, mapId)) {
      try {
        const layerId = req.params.layerId;
        const def = await MapCtrl.readCompositionObj(mapId);
        const currentLayerIndex = def.object.layers?.findIndex(
          (l) => l.metadata.id == layerId
        );
        if (layerId && currentLayerIndex > -1) {
          for (let attr of ["title", "visibility", 'opacity', 'params']) {
            if (req.body[attr] != undefined) {
              def.object.layers[currentLayerIndex][attr] = req.body[attr];
            }
          }
        } else {
          throw new Error("This layer cannot be changed/found for this composition");
        }
        MapCtrl.writeFileQueue(def, { mapId, email });
        distributeMapChange({
          mapId,
          reason: "layer-update",
          layerId,
          definition: def.object,
        });
        return sendRequestCompleted(res);
      } catch (ex) {
        sendInternalError(res, ex);
      }
    } else {
      return sendAccessDenied(res);
    }
  }

  static async styleLayer(req, res) {
    const mapId = req.params.mapId;
    const email = req.session.user;
    log("MAPCTRL[styleLayer]", { mapId });
    if (await MapPrivilegeCtrl.canIWrite(req, mapId)) {
      try {
        const layerId = req.params.layerId;
        let file = req.file;
        const layerFilename = config.uploadDir + file.filename;
        const layerData = await readFile(layerFilename, "utf8");
        const layerObj = JSON.parse(layerData);
        const def = await MapCtrl.readCompositionObj(mapId);
        const currentLayerIndex = def.object.layers?.findIndex(
          (l) => l.metadata?.id == layerId
        );
        if (layerId != undefined && currentLayerIndex > -1) {
          def.object.layers[currentLayerIndex] = layerObj;
        } else {
          throw new Error("This layer cannot be styled for map composition");
        }
        MapCtrl.writeFileQueue(def, { mapId, email });
        distributeMapChange({
          mapId,
          reason: "layer-style",
          layerId,
          definition: def.object,
        });
        fs.unlink(layerFilename, () => {});
        return sendRequestCompleted(res);
      } catch (ex) {
        sendInternalError(res, ex);
      }
    } else {
      return sendAccessDenied(res);
    }
  }
  static async updateMetadata(req, res) {
    const email = req.session.user;
    const mapId = req.params.mapId;
    log("MAPCTRL[updateMetadata]", { mapId });
    if (await MapPrivilegeCtrl.canIWrite(req, mapId)) {
      try {
        const def = await MapCtrl.readCompositionObj(mapId);
        const body = JSON.parse(JSON.stringify(req.body));
        delete body.file; //We don't want to give users chance take control over somebody elses map
        if (body.defaultRights) {
          MapPrivilegeCtrl.setMapDefault(req, res);
        } else {
          if (body.title || body.abstract || body.extent) {
            const compItem = await MapModel.findById(mapId).exec();
            Object.assign(compItem, body);
            compItem.save();
          }
          Object.assign(def.object, body);
          MapCtrl.writeFileQueue(def, { mapId, email });
          sendRequestCompleted(res);
        }
      } catch (ex) {
        sendInternalError(res, ex);
      }
    } else {
      sendAccessDenied(res);
    }
  }
  static async updateMap(req, res) {
    const email = req.session.user;
    const mapId = req.params.mapId;
    log("MAPCTRL[updateMap]", { mapId });
    if (await MapPrivilegeCtrl.canIWrite(req, mapId)) {
      try {
        const def = await MapCtrl.readCompositionObj(mapId);
        const body = req.body;
        if (body.current_base_layer) {
          Object.assign(def.object, body);
          MapCtrl.writeFileQueue(def, { mapId, email });
          if (req.body.layers?.length > 0) {
            for (const layer of req.body.layers) {
              if (layer.features?.features?.length > 0) {
                gju.insertFeatures(
                  mapId,
                  layer.metadata.id,
                  layer.features,
                  def.object.projection
                );
              }
            }
          }

          distributeMapInteraction({
            mapId,
            definition: def,
            reason: "map-update",
          });

          sendRequestCompleted(res);
        } else {
          sendBadRequest(res, `Something went wrong`);
        }
      } catch (ex) {
        sendInternalError(res, ex);
      }
    } else {
      sendAccessDenied(res);
    }
  }
  static async updateLayerOrder(req, res) {
    const email = req.session.user;
    const mapId = req.params.mapId;
    log("MAPCTRL[updateLayerOrder]", { mapId });
    if (!(await MapPrivilegeCtrl.canIWrite(req, mapId))) {
      sendAccessDenied(res);
      return;
    }
    try {
      const def = await MapCtrl.readCompositionObj(mapId);
      const body = JSON.parse(JSON.stringify(req.body));
      if (body && body.length == def.object.layers.length) {
        const tmpNewList = body
          .map((layerId) =>
            def.object.layers.find(
              (existingLayer) => existingLayer.metadata.id == layerId
            )
          )
          .filter((newListItem) => newListItem != undefined);
        if (tmpNewList.length != def.object.layers.length) {
          sendBadRequest(
            res,
            `Some layers couldn't be found using the passed IDs. IDs (${tmpNewList.length}) should match layers metadata.id property (${def.object.layers.length})`
          );
          return;
        }
        def.object.layers = tmpNewList;
        MapCtrl.writeFileQueue(def, { mapId, email });
        sendRequestCompleted(res);
        distributeMapChange({
          mapId,
          reason: "layer-order-change",
          definition: def.object,
        });
      } else {
        sendBadRequest(
          res,
          `Number of layers in request (${body.length}) doesn't match layers in composition (${def.object.layers.length}) or no layers specified`
        );
      }
    } catch (ex) {
      sendInternalError(res, ex);
    }
  }

  static async listTrash(req, res) {
    try {
      log("MAPCTRL[listTrash]");
      const filter =
        req.session.role === Role.admin
          ? {}
          : { user: req.session.user, right: Right.own };
      const privileges = await PrivilegeModel.find(filter).exec();
      const mapIdsOwned = privileges.map((p) => p.map.toString());
      const maps = await MapModel.find({
        _id: { $in: mapIdsOwned },
        deleted: true,
      }).exec();
      sendSuccessData(res, maps);
    } catch (ex) {
      sendInternalError(res, ex);
    }
  }

  static async listShared(req, res) {
    log("MAPCTRL[listShared]");
    try {
      const privileges = await PrivilegeModel.find({
        user: req.session.user,
        right: { $in: [Right.read, Right.write], $ne: Right.own },
      }).exec();
      const mapIds = privileges.map((p) => p.map.toString());
      const maps = await MapModel.find({
        _id: { $in: mapIds },
        deleted: { $ne: true },
      }).exec();
      sendSuccessData(res, maps);
    } catch (ex) {
      sendInternalError(res, ex);
    }
  }

  static async moveToTrash(req, res) {
    log("MAPCTRL[moveToTrash]");
    try {
      const mapId = req.params.mapId;
      if (mapId !== undefined) {
        const compItem = await MapModel.findById(mapId).exec();
        const rights = await MapPrivilegeCtrl.get(req.session.user, mapId);
        if (req.session.role === Role.admin || rights === Right.own) {
          compItem.deleted = true;
          compItem.save();
          distributeMapInteraction({
            mapId,
            reason: "map-remove",
          });
          sendSuccessData(res, compItem);
        } else {
          sendAccessDenied(res);
        }
      } else {
        sendNotFound(res, "Composition not found");
      }
    } catch (ex) {
      sendInternalError(res, ex);
    }
  }

  static async restoreFromTrash(req, res) {
    log("MAPCTRL[restoreFromTrash]");
    try {
      let mapId = req.params.mapId;
      if (mapId !== undefined) {
        const compItem = await MapModel.findById(mapId).exec();
        const rights = await MapPrivilegeCtrl.get(req.session.user, mapId);
        if (req.session.role === Role.admin || rights === Right.own) {
          compItem.deleted = false;
          compItem.save();
          sendRequestCompleted(res);
        } else {
          sendAccessDenied(res);
        }
      } else {
        sendNotFound(res, "Composition not found");
      }
    } catch (ex) {
      sendInternalError(res, ex);
    }
  }

  static async getMetadata(req, res) {
    log("MAPCTRL[getMetadata]");
    try {
      const mapId = req.params.mapId;
      if (await MapPrivilegeCtrl.canIRead(req, mapId)) {
        //can load metadata in any case
        const compItem = await MapModel.findById(mapId)
          .select("title defaultRights abstract")
          .exec();
        sendSuccessData(res, compItem);
      } else {
        sendAccessDenied(res);
      }
    } catch (ex) {
      sendInternalError(res, ex);
    }
  }
}
