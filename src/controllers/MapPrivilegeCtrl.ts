import PrivilegeModel, { Right } from "../types/Privilege";
import MapModel from "../types/Map";
import {
  sendAccessDenied,
  sendBadRequest,
  sendInternalError,
  sendNotFound,
  sendRequestCompleted,
  sendSuccessData,
} from "../utils/responses";
import { AuthContext } from "../auth-providers/auth-context";
import UserModel, { Role } from "../types/User";
import { log } from "../utils/logs";
import mongoose from 'mongoose';

export class MapPrivilegeCtrl {
  static async revoke(req, res) {
    log("MAPPRIVCTRL[revoke]");
    try {
      const mapId = req.params.mapId;
      if (await MapPrivilegeCtrl.canIWrite(req, mapId)) {
        await PrivilegeModel.findByIdAndDelete(req.params.privilegeId).exec();
        sendRequestCompleted(res);
      } else {
        sendAccessDenied(res);
      }
    } catch (ex) {
      sendInternalError(res, ex);
    }
  }

  public static async get(userId, mapId): Promise<Right> {
    log("MAPPRIVCTRL[get]");
    const privileges = await PrivilegeModel.find({
      user: userId,
      map: mapId,
    }).exec();
    const userRights = privileges.map((p) => p.right);
    return userRights.length > 0 ? userRights[0] : undefined;
  }
  /**
   * 
   * @param emailOrId - Requested user email or user id
   * @param getEmail - If set to true, expected return should be an email, if user is found
   * Finds registered user based on email or user id and returns users id or email
   * @returns 
   */
  static async getValidUser(emailOrId: string, getEmail?: boolean): Promise<string> {
    if (mongoose.Types.ObjectId.isValid(emailOrId)) {
      const userRecord = await UserModel.findById(emailOrId)
        .select("_id email")
        .exec();
      return userRecord ? (getEmail ? userRecord.email : userRecord._id) : emailOrId;
    } else {
      const userRecord = await UserModel.findOne({ email: emailOrId })
        .select("_id email")
        .exec();
      return userRecord ? (getEmail ? userRecord.email : userRecord._id) : emailOrId;
    }
  }

  static async grant(req, res) {
    try {
      log("MAPPRIVCTRL[grant]");
      const user = await MapPrivilegeCtrl.getValidUser(req.body.user, true);

      const right = (req.body.right as String).toLowerCase() || Right.read;
      if ((<any>Object).values(Right).includes(right) == false) {
        return MapPrivilegeCtrl.sendBadPrivilegeError(res);
      }
      const mapId = req.params.mapId;
      if (
        user !== undefined &&
        (await MapPrivilegeCtrl.canIWrite(req, mapId))
      ) {
        const found = await PrivilegeModel.find({ user, map: mapId }).exec();
        if (found.length == 0 && user != req.session.user) {
          await PrivilegeModel.create({
            map: mapId,
            user,
            right,
          });
        } else {
          return sendBadRequest(
            res,
            "This user already has access to this map composition"
          );
        }
        sendRequestCompleted(res);
      } else {
        sendAccessDenied(res);
      }
    } catch (ex) {
      sendInternalError(res, ex);
    }
  }
  static async update(req, res) {
    log("MAPPRIVCTRL[update]");
    try {
      const privilegeId = req.body.privilegeId;
      const right = (req.body.right as String).toLowerCase();
      const mapId = req.params.mapId;
      if ((<any>Object).values(Right).includes(right) == false) {
        return MapPrivilegeCtrl.sendBadPrivilegeError(res);
      }
      if (
        privilegeId !== undefined &&
        (await MapPrivilegeCtrl.canIWrite(req, mapId))
      ) {
        const found = await PrivilegeModel.find({
          _id: privilegeId,
          map: mapId,
        }).exec();
        if (found.length > 0) {
          await PrivilegeModel.findOneAndUpdate(
            {
              map: mapId,
              _id: privilegeId,
            },
            { right }
          );
        } else {
          sendNotFound(res, "Map user not found");
        }
        sendRequestCompleted(res);
      } else {
        sendAccessDenied(res);
      }
    } catch (ex) {
      sendInternalError(res, ex);
    }
  }

  static sendBadPrivilegeError(res) {
    //log('MAPPRIVCTRL[sendBadPrivilegeError]');
    sendBadRequest(
      res,
      `Bad permission. It should be one of: ${Object.values(Right)}`
    );
  }

  static async setMapDefault(req, res) {
    try {
      log("MAPPRIVCTRL[setMapDefault]");
      const body = JSON.parse(JSON.stringify(req.body));
      delete body.file; //We don't want to give users chance take control over somebody elses map
      const mapId = req.params.mapId;
      if ((<any>Object).values(Right).includes(body.defaultRights) == false) {
        return MapPrivilegeCtrl.sendBadPrivilegeError(res);
      }
      const userRights = await MapPrivilegeCtrl.get(req.session.user, mapId);
      if (userRights === Right.own || req.session.role === Role.admin) {
        const compItem = await MapModel.findById(mapId).exec();
        compItem.defaultRights = body.defaultRights || Right.restrict;
        delete body.defaultRights;
        compItem.save();
        sendRequestCompleted(res);
      } else {
        sendAccessDenied(res);
      }
    } catch (ex) {
      sendInternalError(res, ex);
    }
  }

  /**
   * Gets permissions for a user to work with a map taking into account maps
   * default access rights granted to anonymous users
   * @param user
   * @param mapId
   */
  static async getSimplified(user: string, mapId: string): Promise<Right> {
    log("MAPPRIVCTRL[getSimplified]");
    const userRights = await MapPrivilegeCtrl.get(user, mapId);
    const compItem = await MapModel.findById(mapId).exec();
    switch (
      userRights ? userRights : compItem.defaultRights || Right.restrict
    ) {
      case Right.own:
        return Right.write;
      case "edit": //Backwards compatibility
      case Right.write:
        return Right.write;
      case Right.restrict:
        return Right.restrict;
      default:
        return Right.read;
    }
  }
  static async canIWrite(req: AuthContext, mapId: string): Promise<boolean> {
    log("MAPPRIVCTRL[canIWrite]");
    const email = req.session?.user || req.client?.user;
    const userRights = await MapPrivilegeCtrl.getSimplified(email, mapId);
    const role = req.session?.role || req.client?.role;
    return role === Role.admin || userRights === Right.write;
  }

  static async canIRead(req: AuthContext, mapId: string): Promise<boolean> {
    log("MAPPRIVCTRL[canIRead]");
    const email = req.session?.user || req.client?.user;
    const userRights = await MapPrivilegeCtrl.getSimplified(email, mapId);
    const role = req.session?.role || req.client?.role;
    return (
      role === Role.admin ||
      (userRights !== undefined && userRights != Right.restrict)
    );
  }

  static async list(req, res) {
    log("MAPPRIVCTRL[list]");
    const mapId = req.params.mapId;
    if (await MapPrivilegeCtrl.canIRead(req, mapId)) {
      const users = await PrivilegeModel.find({ map: req.params.mapId }).exec();
      sendSuccessData(res, users);
    } else {
      sendAccessDenied(res);
    }
  }
}
