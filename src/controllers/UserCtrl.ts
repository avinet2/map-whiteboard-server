import { Role } from "../types/User";
import UserModel from "../types/User";
import {
  sendAccessDenied,
  sendBadRequest,
  sendInternalError,
  sendNotFound,
  sendRequestCompleted,
  sendSuccessData,
} from "../utils/responses";
import fs = require("fs");
import { log } from "../utils/logs";
import PrivilegeModel from "../types/Privilege";
import { activeSocketsArray } from "../app";
import { validationResult } from "express-validator";

const util = require("util");
const readFile = util.promisify(fs.readFile);

function distributeUserRemoval(data: { user: string; username?: string }) {
  log("MAPCTRL[distributeUserRemoval]");
  const foundUsers = activeSocketsArray.filter(
    (userSocket) =>
      userSocket.user == data.user && userSocket.username == data.username
  );
  for (const user of foundUsers) {
    UserCtrl.io.to(user.socketId).emit("user-removed", data);
  }
}
export class UserCtrl {
  static io;

  static async register(req, res) {
    const errors = validationResult(req);
    log("USERS[register]");
    if (!errors.isEmpty()) {
      return sendInternalError(res, errors.array());
    }
    try {
      const user = new UserModel({
        username: req.body.username,
        email: req.body.email,
      });
      user.setPassword(req.body.password);
      await user.save();
      return sendRequestCompleted(res);
    } catch (ex) {
      sendInternalError(res, ex);
    }
  }

  static async list(req, res) {
    log("USERS[list]");
    try {
      if (
        req.session.role === Role.admin ||
        req.session.role === Role.moderator
      ) {
        const users = await UserModel.find()
          .select("email username _id role")
          .exec();
        sendSuccessData(res, users);
      } else {
        sendAccessDenied(res);
      }
    } catch (ex) {
      sendInternalError(res, ex);
    }
  }

  static async find(req, res) {
    log("USERS[find]");
    try {
      if (req.session.role) {
        const users = await UserModel.find({
          $or: [
            { username: { $regex: new RegExp(req.params.query, "ig") } },
            { email: { $regex: new RegExp(req.params.query, "ig") } },
          ],
        })
          .select("username _id")
          .exec();
        sendSuccessData(res, users);
      } else {
        sendAccessDenied(res);
      }
    } catch (ex) {
      sendInternalError(res, ex);
    }
  }
  static async delete(req, res) {
    log("USERS[delete]");
    try {
      if (req.session.role === Role.admin) {
        const user = await UserModel.findByIdAndDelete(
          req.params.userId
        ).exec();
        if (user) {
          await PrivilegeModel.deleteMany({ user: user.email.toString() });
          distributeUserRemoval({
            user: user.email.toString(),
            username: user.username.toString(),
          });
          return sendRequestCompleted(res);
        } else {
          sendNotFound(res, "User not found");
        }
      } else {
        return sendAccessDenied(res);
      }
    } catch (ex) {
      sendInternalError(res, ex);
    }
  }

  static async changeRole(req, res) {
    log("USERS[changeRole]");
    const newRole = req.body.newRole;
    const username = req.body.username;
    try {
      if (req.session.role === Role.admin) {
        const user = await UserModel.findOne({
          $or: [{ username: username }, { email: username }],
        }).exec();
        if (user) {
          user.role = newRole;
          user.save();
          return sendRequestCompleted(res);
        } else {
          sendNotFound(res, "User not found");
        }
      } else {
        return sendAccessDenied(res);
      }
    } catch (ex) {
      sendInternalError(res, ex);
    }
  }

  static async uploadProfileImg(req, res) {
    log("USERS[uploadProfileImg]");
    if (!req.file) {
      return sendBadRequest(res, "No files were uploaded");
    }
    if (!req.session?.user) {
      return sendAccessDenied(res);
    }

    if (req.session?.avatar) {
      try {
        fs.unlink("./public" + req.session.avatar, () => {
          req.session.avatar = undefined;
        });
      } catch (ex) {
        sendInternalError(res, ex);
      }
    }
    const file = req.file;

    const avatar = "/images/profile/" + file.filename;
    try {
      await UserModel.findOneAndUpdate(
        {
          email: req.session.user,
        },
        { avatar }
      );
      req.session.avatar = avatar;
      sendSuccessData(res, { imagePath: avatar });
    } catch (ex) {
      sendInternalError(res, ex);
    }
  }
}
