import GeoJSONFeatureModel from "./types/GeoJSONFeature";
import { log } from "./utils/logs";
import { applyChange } from 'deep-diff'

var transform = require("proj4geojson");

export module GeoJSONUtilities {
  export function createFeatureCollection(): GeoJSON.FeatureCollection {
    return {
      type: "FeatureCollection",
      features: [],
    };
  }

  export function upsertFeature(
    mapId: string,
    layerId: string,
    feature: GeoJSON.Feature
  ): boolean {
    log("GJUTILS[upsertFeature]");
    if (!feature) return false;
    feature["_map"] = mapId;
    feature["_layer"] = layerId;
    feature["_id"] = feature.properties.id;
    GeoJSONFeatureModel.findOneAndUpdate(
      { _id: feature.properties.id },
      feature,
      { upsert: true },
      (err, res) => {
        if (err) console.error(err);
      }
    );
    return true;
  }

  export async function changeFeature(
    mapId: string,
    layerId: string,
    featureId: string | number,
    differences: any
  ): Promise<boolean> {
    try {
      log("GJUTILS[changeFeature]");
      const feature =  (await GeoJSONFeatureModel.findById(featureId)).toObject();
      if (!feature) return false;   
      if (!differences) return false;   
      for(let d of differences) {
        applyChange(feature, feature, d);
      }
      GeoJSONFeatureModel.findOneAndUpdate(
        { _id: featureId },
        feature,
        { upsert: true },
        (err, res) => {
          if (err) console.error(err);
        }
      );
      return true;
    } catch (ex) {
      debugger;
    }
  }

  export function insertFeatures(
    mapId: string,
    layerId: string,
    features: GeoJSON.FeatureCollection,
    projection: string
  ): boolean {
    try {
      log("GJUTILS[insertFeatures]");
      if (!features?.features) return false;
      if (projection) {
        features = transform.from(features, projection?.trim().toUpperCase());
      }
      for (let feature of features.features) {
        feature["_map"] = mapId;
        feature["_layer"] = layerId;
        feature["_id"] = feature.properties.id;
      }
  
      GeoJSONFeatureModel.insertMany(features.features, (err, res) => {
        if (err) console.error(err);
      });
      return true;
    } catch (ex) {
      console.error(ex);
      throw ex;
    }
  }

  export function deleteOrRestoreFeature(
    featureId: string | number,
    featureDeletion: boolean
  ): boolean {
    if (featureDeletion) {
      log("GJUTILS[deleteFeature]");
    } else {
      log("GJUTILS[restoreFeature]");
    }
    if (!featureId) return false;
    GeoJSONFeatureModel.findOneAndUpdate(
      { _id: featureId },
      { deleted: featureDeletion || false },
      (err, res) => {
        if (err) console.error(err);
      }
    );
  }

  export function arrayToPointFeature(xy: Array<number>): GeoJSON.Feature {
    log("GJUTILS[arrayToPointFeature]");
    return {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: [xy[0], xy[1]],
      },
      properties: {},
    };
  }
}

export default GeoJSONUtilities;
