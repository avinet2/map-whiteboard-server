
import { Credentials } from "../types/Credentials";
import UserModel from "../types/User";
import { AuthContext } from "./auth-context";
import { AuthResult, IAuthProvider } from "./auth-provider.interface";

export class WhiteBoardDbAuthProvider implements IAuthProvider {
  postAuthenticate(context: AuthContext, data: Credentials) {}
  constructor(private options: {}) {}

  async authenticate(context: AuthContext, data: Credentials): Promise<AuthResult> {
    const user = await UserModel.findOne({
      $or: [{ username: data.username }, { email: data.username }],
    }).exec();
    if (data.password === undefined) {
      return;
    }
    if (user && user.validPassword(data.password)) {
      return {user: user.toObject()};
    } else {
      return;
    }
  }
}
