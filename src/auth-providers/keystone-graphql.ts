import { AuthResult, IAuthProvider } from "./auth-provider.interface";
import { AuthContext } from "./auth-context";
import { Credentials } from "../types/Credentials";
import config from "../config";
import fetch from 'node-fetch';

const allowedEndpoints = config.allowedAuthProviders
const https = require("https");

export class KeystoneGraphqlProvider implements IAuthProvider {
  httpsAgent = new https.Agent({
    rejectUnauthorized: false,
  });

  postAuthenticate(context: AuthContext, data: Credentials) {
    /* Do nothing because we set client.user in authenticate function
        to not make unnecessary requests
        */
  }
  constructor(private options: { url: string }) {}

  async authenticate(context: AuthContext, data: Credentials): Promise<AuthResult> {
    var username = data.username;
    var password = data.password;
    const query = `mutation signin($identity: String, $secret: String) {
            authenticate: authenticateUserWithPassword(email: $identity, password: $secret) {
              item {
                id, 
                email,
                name
              }
            }
          }`;
    if (allowedEndpoints.indexOf(this.options.url) == -1) {
      return;
    }
    const res = await fetch(this.options.url, {
      method: "post",
      headers: { "Content-Type": "application/json" },
      agent: this.httpsAgent,
      body: JSON.stringify({
        variables: {
          identity: username,
          secret: password,
        },
        query,
      }),
    });
    const json = res.json();

        const result =
          json.data &&
          json.data.authenticate &&
          typeof json.data.authenticate.item != "undefined";
        if(result){
          return {user: {
            email: json.data.authenticate.item.email, 
            username: json.data.authenticate.item.name
          }};
        } else {
          return;
        }
  }
}
