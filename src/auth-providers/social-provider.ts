import UserModel from "../types/User";
import { Credentials } from "../types/Credentials";
import { AuthContext } from "./auth-context";
import { IAuthProvider, AuthResult } from "./auth-provider.interface";

export class SocialAuthProvider implements IAuthProvider {
  postAuthenticate(context: AuthContext, data: Credentials) {}
  constructor(private options: {}) {}

  async authenticate(context: AuthContext, data: Credentials): Promise<AuthResult> {
    if (!data.email) {
      return;
    }
    const user = await UserModel.findOne({
      email: data.email,
    }).exec();
    if (user) {
      return {user: user.toObject()}
    } else if (data.username && data.email) {
      try {
        const user = new UserModel({
          username: data.username,
          email: data.email,
        });
        await user.save();
        return {user: user.toObject()}
      } catch (error) {
        return { error};
      }
    } else {
      return;
    }
  }
}
