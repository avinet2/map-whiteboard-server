export type AuthUserData = {
  role?: any;
  avatar?: any;
  _id?: any;
  username: String;
  email: String;
};

export type AuthResult = {
  error?: any,
  user?: AuthUserData
};

export interface IAuthProvider {
  authenticate(socket, data): Promise<AuthResult>;
  postAuthenticate(socket: any, data: any);
}

export enum Provider {
  KeystoneGraphql = "keystone-graphql",
  Liferay = "liferay",
  ZeroAuth = "zero",
  WhiteBoardDbAuth = "",
  Facebook = 'FACEBOOK',
  Google = 'GOOGLE',
}
