/**
 * Auth context can be Socket or Request in the case of API call.
 * Will store user info
 */
export interface AuthContext {
  id: any; //For socket contexts
  client?: any; //For socket contexts
  session?: any; //For express req object
  sessionID?: string; //For all
}
